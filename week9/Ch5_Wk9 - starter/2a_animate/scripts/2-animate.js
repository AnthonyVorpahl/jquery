$(function(){
    
    $("#start").click(function(){
        //animates cirlce 250px everytime start is clicked
//        $("#circle").animate({left: "+=250px"});
        
        //animates circle 1000px whenever start is clicked, at 3000ms timer
        $("#circle").animate({left: "+=1000px"}, 3000);
    });    
    
    
    // telling the animation to stop where its located
    $("#stop").click(function(){
        $("#circle").stop(true, false);
        
        // finishes animation, to the end
    //    $("#circle").finish();
        
    });
   
    
});