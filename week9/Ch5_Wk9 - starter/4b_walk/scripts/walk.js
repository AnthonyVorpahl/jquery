$(function () {

    $("#walk").click(function () {

        loop();

    });

    $("#stop").click(function () {
        $(".walking").stop(true,false);
    });

    $("#rewind").click(function () {
        location.reload();
    });


    
    function loop(){
                $(".walking").animate({
                backgroundPosition: "-180",
                marginLeft: ["+=10", "linear"]
            }, 0).delay(100).animate({
                backgroundPosition: "-350",
                marginLeft: ["+=10", "linear"]
            }, 0).delay(100)
            .animate({
                backgroundPosition: "-560",
                marginLeft: ["+=10", "linear"]
            }, 0).delay(100)
            .animate({
                backgroundPosition: "-755",
                marginLeft: ["+=10", "linear"]
            }, 0).delay(100)
            .animate({
                backgroundPosition: "-938",
                marginLeft: ["+=10", "linear"]
            }, 0).delay(100)
            .animate({
                backgroundPosition: "-1120",
                marginLeft: ["+=10", "linear"]
            }, 0).delay(100)
            .animate({
                backgroundPosition: "-1316",
                marginLeft: ["+=10", "linear"]
            }, 0).delay(100)
            .animate({
                backgroundPosition: "0",
                marginLeft: ["+=10", "linear"]
            }, 0,function(){
            loop();
        });
        
    }
    
});
