$(function () {
    
    $("#future").click(function(){
        $("#5k").replaceWith("	<ul>
					<li>Snowball Sprint</br> 1/14/21 </br>Mosquito Hill </br>New London</li>
					<li>Winter Warrior </br>2/06/21 </br>Bay Beach</br> Green Bay</li>
					<li>Big Chill Run </br>2/24/21</br> Mid Valley Golf Course </br>De Pere</li>
				</ul>"
        
    )});
    
    $(".races").hide();
    //5k race click event   show/hide
    if ($("5k").show()) {
        $("#race1").click(function () {
            $("#5k").toggle().next().hide(1000);

            // clear other colums when showing
            $("#half").slideUp(1000);
            $("#full").fadeOut(1000);
        });
    } else {
        $("#5k").hide();
    }
    //half marathon race click events     slide
    if ($("half").slideDown()) {
        $("#race2").click(function () {
            $("#half").slideToggle().next().slideUp(1000);

            // clear other colums when showing
            $("#5k").hide();
            $("#full").fadeOut(1000);
        });
    } else {
        $("#half").hide();
    }
    //full marathon click events      fadein/out
    if ($("full").fadeIn(1000)) {
        $("#race3").click(function () {
            $("#full").fadeToggle().next().fadeOut(1000);

            // clear other colums when showing
            $("#half").slideUp(1000);
            $("#5k").hide();
        });
    } else {
        $("#full").fadeOut(1000);

    }

    
    //assign week7 clickable events in links

        // hide dd    
    $("dd").hide();
    
    $("dt").click(function(){
        $(this).toggleClass("open").next().slideToggle();
    });
    if($("mini").show){
        $("#iron").hide();
        $("#twilight").hide();
    }

});
