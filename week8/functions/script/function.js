$(function(){
   //Anonymous Function
    $("#action").click(function(){
        // alert("This Is an Anonymous Function!");
        //MyFuction();
        // _("test");
        //$("#box1").slideToggle("slow",function(){
        //    alert("Sliddddddddddddding");
        //});
        
        // loop change colors to blue
        for(x = 1;x<6;x++){
            $("#id" + x).css("background-color", "blue");
        }
    });
    
    // create own languate, function expression
    var _ = function(var1){
        alert("Take action on var1:" + var1);
    }

    
    // daisy chains
    // change background color as mouse enters id
    $("#id1,#id2,#id3,#id4,#id5").mouseenter(function(){
       $(this).css("background-color", "red");
    });
    // change background color as mouse leaves id
    $("#id1,#id2,#id3,#id4,#id5").mouseleave(function(){
       $(this).css("background-color", "green");
    });
    
    
    
    
});
    //Named Function  does not need jquery  
    function MyFunction(){
        alert("This Is a Named Function!");
    }