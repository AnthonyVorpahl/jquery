$(function(){
    // declare variables
    var x = "<img src='img/x.png' />";
    var o = "<img src='img/o.png' />";
    var player = 1;

    // declare tracking variable
    var p1Arr = new Array();
    var p2Arr = new Array();
    
    function win(currentPlayer){
        // sort array list
        currentPlayer=currentPlayer.sort();
        
        //row 1 across
        if(currentPlayer.includes("td0") && currentPlayer.includes("td1") && currentPlayer.includes("td2")){
            alert("Player" + player + " is the winner!!");
            $("td").unbind("click");
        }
        
        // row 2 across
        else if(currentPlayer.includes("td3") && currentPlayer.includes("td4") && currentPlayer.includes("td5")){
            alert("Player" + player + " is the winner!!");
            $("td").unbind("click");
        }
        
        // row 3 across
        else if(currentPlayer.includes("td6") && currentPlayer.includes("td7") && currentPlayer.includes("td8")){
            alert("Player" + player + " is the winner!!");
            $("td").unbind("click");
        } 
        
        //diag left- right
        else if(currentPlayer.includes("td0") && currentPlayer.includes("td4") && currentPlayer.includes("td8")){
            alert("Player" + player + " is the winner!!");
            $("td").unbind("click");
        }
        
        // diag right-left
        else if(currentPlayer.includes("td2") && currentPlayer.includes("td4") && currentPlayer.includes("td6")){
            alert("Player" + player + " is the winner!!");
            $("td").unbind("click");
        }
        
        //column 1 up/down
        else if(currentPlayer.includes("td0") && currentPlayer.includes("td3") && currentPlayer.includes("td6")){
            alert("Player" + player + " is the winner!!");
            $("td").unbind("click");
        }
        
        //column 2 up/down
        else if(currentPlayer.includes("td1") && currentPlayer.includes("td4") && currentPlayer.includes("td7")){
            alert("Player" + player + " is the winner!!");
            $("td").unbind("click");
        }
        
        // column 3 up/down
        else if(currentPlayer.includes("td2") && currentPlayer.includes("td5") && currentPlayer.includes("td8")){
            alert("Player" + player + " is the winner!!");
            $("td").unbind("click");
        }
        
    }
    
    
    // do calcs and logic
    $("td").click(function(){
       if(player==1){
           
           //selects player 1 to x
            $(this).html(x);
           
           // unclickable
            $(this).unbind("click");
           
           //store data p1
           p1Arr.push(this.id);
           
           //determine who wins
           win(p1Arr);
           
           
           //swap to player 2
           player = 2;
       }else {
           
           //selects player 2 to o
            $(this).html(o);
           
           //store data p2
           p2Arr.push(this.id);
           
           // unclickable
            $(this).unbind("click");
           
           //determine who wins
           win(p2Arr);
           
           //swap player 1
           player = 1
        }
        $("#again").click(function(){
           location.reload();
        });
    });
       
});
