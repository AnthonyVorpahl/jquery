$(function () {
            //slide animations box 1
            $("#slideUp").click(function () {
                $("#box1").slideUp();
            });
            $("#slideDown").click(function () {
                $("#box1").slideDown();
            });
            $("#toggleSlide").click(function () {
                $("#box1").slideToggle();
            });

            // fade animations box2
            $("#fadeOut").click(function () {
                $("#box2").fadeOut(3000);
            });

            $("#fadeIn").click(function () {
                $("#box2").fadeIn('slow');
            });

            $("#toggleFade").click(function () {
                $("#box2").fadeToggle('fast');
            });

            //fadeTo effects
            $("#fadeTo50").click(function () {
                $("#box2").fadeTo(1000, .5);
            });

            $("#fadeTo100").click(function () {
                $("#box2").fadeTo(1000, 1);
            });


            //box 3 hide, show,fade
            $("#hide").click(function () {
                $("#box3").hide();
            });
            $("#show").click(function () {
                $("#box3").show();
            });
            $("#hideShow").click(function () {
                $("#box3").toggle();


            });
     });