$(function () {
    //$("#temp").text("<em>This is Very Cool!!</em>")
    // changing id temp
    $("#temp").html("<em>This is Very Cool!!</em>");
    //changing h1
    $("h1").html("<em>This is Very Cool Page Indeeeeed!!</em>");



    // adding button
    $('<input type="button" value="hide" id="hide" /> ').insertBefore("#temp");

    //click event on button
    $("#hide").click(function () {
        $("p").toggle();

        if ($("p").is(':visible')) {
            $(this).val('Hide');
        } else {
            $(this).val('Show');
        }

    });


    //add stuff to the begining and end of p tag
    $("div").append('<p>Append me to the list</p>');
    
    //add hello to all p tags
//    $("<strong>Hello </strong>").preappendTo('p');
    
    // add to just specific tag
    $("<strong>Hello </strong>").prependTo('#temp');
    
    //adding to end of tag
     $("<em> Goodbye</em>").appendTo('p');
    
    // removes tag with id temp
    $("#temp").remove();
    
    //remove all tags
//    $("p").remove();
    
    //remove ones with specific words CASE SENSITIVE
    $("p").remove(':contains("Goodies")');

});
